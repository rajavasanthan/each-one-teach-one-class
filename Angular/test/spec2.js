describe('inputbox test', function () {
    it('should show error when user type 1 character', function () {
        browser.get('http://localhost/each-one-teach-one-class/Angular/app/#/contact');
        browser.sleep(3000);
        element(by.model('user.name')).sendKeys('h');
        var elm = element(by.id('name_error'));
        expect(elm.getText()).toEqual('Name length should not exeed 8 characters');
    });

    it('should not show error message when the character length is more than 1', function () {
        element(by.model('user.name')).sendKeys('asanth');        
        var ele = element(by.id("name_error"));
        expect(hasClass(ele, 'ngHide')).toBe(true);
    });

    it('should show error when user type more than 8 character', function () {
        browser.sleep(3000);
        element(by.model('user.name')).sendKeys('hffdssfsferfdfderdfrdsfdf');
        var elm = element(by.id('name_error'));
        expect(elm.getText()).toEqual('Name length should not exeed 8 characters');
    });

    it('should enable submit button',function(){
        element(by.model('user.age')).sendKeys(20);
        element(by.model('user.email')).sendKeys('rajavasanthan@gmail.com');
        element(by.model('user.password')).sendKeys('123');
        element(by.model('user.re_password')).sendKeys('123');
        var select = element(by.model('user.country_id'));
        select.$('[value="number:1"]').click();
        element(by.buttonText('Submit')).getAttribute('disabled').then(function(value){
            console.log(value);
            expect(value).not.toEqual(true);
        });
        browser.sleep(3000);
    });

});


var hasClass = function(ele, cls){
    return ele.getAttribute('class').then(function(classes){
        return classes.split(" ").indexOf(cls) == -1;
    })
}