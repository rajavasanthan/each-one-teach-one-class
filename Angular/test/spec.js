describe('inputbox test', function() {
  it('should show error when user type 1 character', function() {
    browser.get('http://localhost/each-one-teach-one-class/Angular/app/#/contact');
    browser.sleep(2000);
    element(by.model('user.name')).sendKeys('v');
    var elm = element(by.id("name_error"));
    expect(elm.getText()).toEqual("Name length should not exeed 8 characters");
    
  });

  it('should not show error message when the character length is more than 1',function(){
    element(by.model('user.name')).sendKeys('asanth');
    var ele = element(by.id("name_error"));
    expect(hasClass(ele, 'ngHide')).toBe(true);
  });

  it('should show error message when the character length is more than 8',function(){
    element(by.model('user.name')).sendKeys('arajan');
    var elm = element(by.id("name_error"));
    expect(elm.getText()).toEqual("Name length should not exeed 8 characters");
  });

  it('should enable if all the field is entered',function(){
    element(by.model('user.age')).sendKeys(25);
    element(by.model('user.email')).sendKeys('arajan');
    element(by.model('user.password')).sendKeys('arajan');
    element(by.model('user.re_password')).sendKeys('arajan');
    var select = element(by.model('user.country_id'));
    select.$('[value="number:1"]').click();
    element(by.buttonText('Submit')).getAttribute('disabled').then(function(value){
      expect(value).not.toEqual(true);
    });
  });
});
var hasClass = function (ele, cls) {
    return ele.getAttribute('class').then(function (classes) {
        return classes.split(' ').indexOf(cls) == -1;
    });
};

var selectDropdownbyNum = function ( ele, optionNum ) {
  if (optionNum){
    var options = ele.findElement(by.tagName('option'))   
      .then(function(options){
        options[optionNum].click();
      });
  }
};