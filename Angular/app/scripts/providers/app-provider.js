app.provider("application",[function () {
	var appTitle;


	return {
		setAppTitle : function(title){
			appTitle = title;
		},
		$get : function(){
			return {
				title : appTitle
			}
		}
	}
}]);