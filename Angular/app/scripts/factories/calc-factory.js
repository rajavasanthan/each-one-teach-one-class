app.factory("CalcFactory",['$rootScope',function($rootScope) {
	var myval =  "value";
	return {
		add : function(a,b){
			return a+ b;
		},
		multiply : function(a,b){
			return a * b;
		},
		divide : function(a,b){
			return a / b;
		},
		sub : function(a,b){
			return a - b;
		}
	}
}]);