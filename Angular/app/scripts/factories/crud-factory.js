app.factory("CrudFactory",["$rootScope","$http","$q",function($rootScope,$http,$q){
	return {
		get : function(url){
			var deferred = $q.defer();
			$http({
				method : 'GET',
				url : $rootScope.baseUrl + url
			}).success(function(result){
				deferred.resolve(result);
			}).error(function(err){
				deferred.reject(err);
			});
			return deferred.promise;
		},
		post : function(url,data){
			var deferred = $q.defer();
			$http({
				method : 'POST',
				url : $rootScope.baseUrl + url,
				data : data
			}).success(function(result){
				deferred.resolve(result);
			}).error(function(err){
				deferred.reject(err);
			});
			return deferred.promise;
		},
		put : function(url,data){
			var deferred = $q.defer();
			$http({
				method : 'PUT',
				url : $rootScope.baseUrl + url,
				data : data
			}).success(function(result){
				deferred.resolve(result);
			}).error(function(err){
				deferred.reject(err);
			});
			return deferred.promise;
		},
		delete : function(url){
			var deferred = $q.defer();
			$http({
				method : 'DELETE',
				url : $rootScope.baseUrl + url,
			}).success(function(result){
				deferred.resolve(result);
			}).error(function(err){
				deferred.reject(err);
			});
			return deferred.promise;
		}
	}
}]);