app.factory("HttpInterceptors",['$rootScope',function($rootScope) {
	return {
		request : function(config){
			// console.log(config);
			return config;
		},
		response : function(config){
			// console.log(config);
			return config;
		},
		responseError : function(config){
			if(config.status === 500){
				console.log("Internal Server Error");
			}
		}
	}
}]);