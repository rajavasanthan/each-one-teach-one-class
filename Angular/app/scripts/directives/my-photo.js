app.directive("profilePhotoIsHere",function(){
	return {
		restrict : "E",
		templateUrl : "views/profile-photo.html",
		link : function(scope,element,attribute){
			if(attribute.size === "mid"){
				scope.imageSize = "150px";
			}

			if(attribute.size === "large"){
				scope.imageSize = "450px";
			}
		}
	};
});