// Single Page Application (SPA)
// Data Binding
// Async


// Module
// - Views
// - Directives
// - Services
// - Factories
// UI-Router


// value		|
// constant		|
// factory		|  Service Recepi
// Services 	|
// provider 	|


// Protactor Testing
// HTTP intersepetors
// Advance Directive
// ng-resource


// Empty Space remove
// Remove Comments
// Change Vaiable names
var app = angular.module("myApp",[
	'ui.router',
	'ngAnimate', 
	'toastr',
	'satellizer',
	'ngResource'
	]).filter('reverse',function(){
    return function(string){
        return string;
    }
});;

app.config(["$stateProvider","$urlRouterProvider","applicationProvider","toastrConfig","$authProvider","$httpProvider",function($stateProvider,$urlRouterProvider,applicationProvider,toastrConfig,$authProvider,$httpProvider){
	

	$httpProvider.interceptors.push('HttpInterceptors');
	applicationProvider.setAppTitle('My New Project');
	$authProvider.loginUrl = 'http://symphonyapi.gridsandguides.com/authenticate';
	$stateProvider.state("home",{
		"url" : "/home",
		"templateUrl" : "views/home.html",
		"controller" : "HomeController"
	})
	.state("login",{
		"url" : "/login",
		"templateUrl" : "views/login.html",
		"controller" : "LoginController"
	})
	.state("about",{
		"url" : "/about",
		"templateUrl" : "views/about.html",
		"controller" : "AboutController"
	})
	.state("contact",{
		"url" : "/contact",
		"templateUrl" : "views/contact.html",
		"controller" : "ContactController"
	})
	.state("admin",{
		"url" : "/admin",
		"templateUrl" : "views/admin.html",
		"controller" : "AdminController",
		"resolve" : {
			authenticate : function($auth){
				return $auth.isAuthenticated();
			}
		}
	})
	.state("admin.dashboard",{
		"url" : "/dashboard",
		"templateUrl" : "views/dashboard.html",
		"controller" : "DashboardController"
	})
	.state("admin.groupById",{
		"url" : "/group/:id",
		"templateUrl" : "views/group-by-id.html",
		"controller" : "GroupByIdController"
	})
	.state("admin.stock",{
		"url" : "/stock",
		"templateUrl" : "views/stock.html",
		"controller" : "StockController"
	})
	.state("admin.user",{
		"url" : "/user",
		"templateUrl" : "views/user.html",
		"controller" : "UserController"
	})
	.state("admin.add_user",{
		"url" : "/add",
		"templateUrl" : "views/create-user.html",
		"controller" : "CreateUserController"
	})
	.state("admin.edit_user",{
		"url" : "/edit/:id",
		"templateUrl" : "views/edit-user.html",
		"controller" : "EditUserController"
	})
	;

	$urlRouterProvider.otherwise("/home");
}]);

app.constant("url",{
	name : "vasanth"
});
app.value('new_val','This new Val');

app.run(['$rootScope',function($rootScope){
	$rootScope.baseUrl = "http://symphonyapi.gridsandguides.com";
	$rootScope.awsurl = "https://s3-ap-southeast-1.amazonaws.com/symphony-assets/";
}]);

app.filter('myFilter',function(){
	return function(x){
		return "Album - " + x;
	}
});

app.filter('indianCurrencyFilter',function(){
	return function(price){
		var x=price;
		x=x.toString();
		var afterPoint = '';
		if(x.indexOf('.') > 0)
   			afterPoint = x.substring(x.indexOf('.'),x.length);
			x = Math.floor(x);
			x=x.toString();
			var lastThree = x.substring(x.length-3);
			var otherNumbers = x.substring(0,x.length-3);
			if(otherNumbers != '')
    			lastThree = ',' + lastThree;
				var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree + afterPoint;
				return res;
	}
});

