app.controller("LoginController",["$scope","$auth","$state",function($scope,$auth,$state) {
	$scope.login = function(){
		$auth.login({
			"email" : $scope.user.username,
			"password" : $scope.user.password
		}).then(function(res){
		$state.go("admin.user");			
		},function(err){
			alert("Something went wrong");
		});
	};
}]);