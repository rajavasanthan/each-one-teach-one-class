app.controller("EditUserController",["$scope","CrudFactory","$state","$stateParams","toastr",function ($scope,CrudFactory,$state,$stateParams,toastr) {
	$scope.submitValue = "Submit";
	CrudFactory.get('/user/' + $stateParams.id).then(function(res){
		$scope.user = res.result;
	},function(err){
		console.log(err);
	});


	$scope.editUser = function(){
		CrudFactory.put('/user/' + $stateParams.id,$scope.user).then(function(res){
			toastr.success('Updated', 'User Updated Successfully!');
			$state.go("admin.user");
		},function(err){
			console.log(err);
		});
	};

}]);