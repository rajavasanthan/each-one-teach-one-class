app.controller("UserController",["$scope","CrudFactory",function ($scope,CrudFactory) {
	CrudFactory.get("/user").then(function(res){
		$scope.users = res.result;
	},function(err){
		alert("Something went wrong!");
	});

	$scope.deleteUser = function(index){
		var result = confirm("Are you sure do you Want to Delete this?");
		if(result){
			CrudFactory.delete("/user/" + index).then(function(res){

			},function(err){

			});
		}
	};
}]);