app.controller("HomeController",['$rootScope','$scope','CrudFactory','$resource',function($rootScope,$scope,CrudFactory,$resource){
	$scope.password = '';
  $scope.grade = function() {
    var size = $scope.password.length;
    if (size > 8) {
      $scope.strength = 'strong';
    } else if (size > 3) {
      $scope.strength = 'medium';
    } else {
      $scope.strength = 'weak';
    }
  };

  
	CrudFactory.get('/top_artist').then(function(result){ 
		$scope.artist = result.artist; 
		console.log($scope.artist); 
	},function(err){
		console.log(err);
		alert('Some thing went Wrong');
	});

	$scope.name = "Vasanth";
	$scope.userDetails = {
		name : "Name",
		age : 23
	}

	var User = $resource('http://symphonyapi.gridsandguides.com/user/:userId',{userId:'@id'});
	var user = User.get({userId:17},function(){
		user.$save();
	});

}]);