app.controller("CreateUserController",["$scope","CrudFactory","$state",function ($scope,CrudFactory,$state) {
	$scope.submitValue = "Submit";
	$scope.postData = function(){
		CrudFactory.post("/post_user",{
			"name" : $scope.user.name,
    		"email" : $scope.user.email,
    		"password" : $scope.user.password
		}).then(function(res){
			$state.go("admin.user");
		},function(err){
			alert("Some thing went wrong");
		});
	};
}]);