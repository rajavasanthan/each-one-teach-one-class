app.controller("AdminController",["$scope","authenticate","$state","$auth","$rootScope",function($scope,authenticate,$state,$auth,$rootScope){
	if(!authenticate){
		$state.go("login");
	}

	$scope.logout = function(){
		$auth.logout();
		$state.go("login");
	}

	// $rootScope.$on('myEvent',function(event,data){
	// 	alert(data);
	// });
}]);