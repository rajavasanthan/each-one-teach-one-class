var number = "1283.4";



// parseInt
document.getElementById("output").innerHTML = parseInt(number);

// toString
document.getElementById("output").innerHTML = number.toString(16);

// NAN
document.getElementById("output").innerHTML = 2 / 'Hello';

//parseFloat
document.getElementById("output").innerHTML = parseFloat(number);
