
	var this_is_my_variable = 1;
	var thisIsMyVariable = 1;
	
	// String
	var mySringVariable = "Hello";

	// Integer
	var myIntegerVariable = 2523;

	// Float
	var myFloat = 12.56;

	// Object
	var myObject = {
		"name" : "Vasanth",
		"age"  : 28,
		"salary" : 56000.25689
	}

	// Boolean
	var myBool = true;

	var myArray = ["Hello How are you",265,23.25,{"name" : "vasanth","age" : 28}];

	var peoples = [
		{
			"name" : "Vasanth",
			"age"  : 28,
			"dob"  : "07-07-1988",
			"city" : "chennai",
			"maritalStatus" : true,
			"phoneNumber" : [
				{
					"numberType" : 1,
					"number" : 988565856
				},
				{
					"numberType" : 2,
					"number" : 988565869	
				}
			],
			"address" : [
				{
					"type" : "Home",
					"address_line_1" : "No 159 Gandhi Street",
					"address_line_2" : "villivakkam",
					"city" : "chennai",
					"state" : "Tamil Nadu",
					"pincode" : 6225352,
					"country" : "India"
				},
				{
					"type" : "Business",
					"address_line_1" : "No 159 Gandhi Street",
					"address_line_2" : "villivakkam",
					"city" : "chennai",
					"state" : "Tamil Nadu",
					"pincode" : 6000235,
					"country" : "India"
				}
			]
		},
		{
			"name" : "Ramesh",
			"age"  : 28,
			"dob"  : "07-07-1988",
			"city" : "chennai",
			"maritalStatus" : true,
			"phoneNumber" : [
				{
					"numberType" : 1,
					"number" : 988565856
				},
				{
					"numberType" : 2,
					"number" : 988565869	
				}
			],
			"address" : [
				{
					"type" : "Home",
					"address_line_1" : "No 159 Gandhi Street",
					"address_line_2" : "villivakkam",
					"city" : "chennai",
					"state" : "Tamil Nadu",
					"pincode" : 6225352,
					"country" : "India"
				},
				{
					"type" : "Business",
					"address_line_1" : "No 159 Gandhi Street",
					"address_line_2" : "villivakkam",
					"city" : "chennai",
					"state" : "Tamil Nadu",
					"pincode" : 6000235,
					"country" : "India"
				}
			]
		}
	];

	var data = {
	"movie_name" : "sdsd",
	"release_date" : 1568,
	"rating" : 5,
	"rating_out_of" : 10,
	"num_of_ratings" : 146,
	"duration" : "1h 30min",
	"cover_image" : "http://placehold.it/800x800",
	"trailer" : "https://youtube.com/watch?=fdfderere",
	"description" : "dfssvsdfsd fsdhfsdj dsbvfgdgfvdsbf dhvfsdbfdsfds",
	"crew" : {
		"director" : "fdfdfd",
		"writer" : "dfdfdf"
	},
	"casting" : [
		{
			"id" : 23,
			"real_name" : "cvcvcvc",
			"screen_name" : {
				"name" : "dsdssd",
				"type" : null
			},
			"image" : "http://dfdfdfd/vcv",
			"order" : 0
		},
		{
			"id" : 26,
			"real_name" : "cvcvcvc",
			"screen_name" : {
				"name" : "dsdssd",
				"type" : "voice"
			},
			"image" : "http://dfdfdfd/vcv",
			"order" : 1
		}
	],
	"stars" : [
		{
			"name" : "Ben Winchell",
			"id" : 25335
		}
	]
}

	