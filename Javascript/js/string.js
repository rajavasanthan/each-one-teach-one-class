var myString = "Hello how are you you";

// length
document.getElementById("output").innerHTML = myString.length;

// indexOf
document.getElementById("output").innerHTML = myString.indexOf('how');

// lastIndexOf
document.getElementById("output").innerHTML = myString.lastIndexOf('o');

// search
document.getElementById("output").innerHTML = myString.search('you');	

// search
document.getElementById("output").innerHTML = myString.slice(2,16);

// replace
document.getElementById("output").innerHTML = myString.replace('how','vasanth');

// upcase
document.getElementById("output").innerHTML = myString.toUpperCase();

// CharacterAt
document.getElementById("output").innerHTML = myString.charAt(7);

// split
document.getElementById("output").innerHTML = myString.split(" ");
console.log(myString);
console.log(myString.split(" "));